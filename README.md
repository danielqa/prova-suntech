# prova-suntech

Projeto de avaliação da Suntech.


## Instruções

### Para rodar a aplicação, é preciso somente ter instalado:

* Docker
* Docker Compose

### Clone o projeto na pasta desejada (repositório público)

```sh
$ git clone https://danielqa@bitbucket.org/danielqa/prova-suntech.git
```

### Entre na pasta criada e rode o comando:

```sh
$ docker-compose up
```

Obs.: Aguarde até que as 3 instâncias Spring Boot tenham inicializado

### Após concluído o processo, basta acessar no browser a url:

=> http://localhost

### Para acesso externo do banco, se necessário, basta acessar com os dados:

- url: 'jdbc:postgresql://localhost:8001/prova-suntech'
- username: 'postgres'
- password: 'postgres'


## Pontos de melhoria

* Criar testes automatizados (integração, unitário, end-to-end)
* Testar o plugin 'Liquibase Hibernate Integration', buscando unificar a configuração dos dados de conexão com o banco
* Refatorar o projeto front-end, quebrando o template em componentes menores e mais sucintos


## Pontos positivos

* Arquitetura simples, funcional e produtiva, fazendo uso de tecnologias de ponta como Spring Boot, Spring Data, Gradle, Liquibase, Docker, Nginx e React
* Padronização, clean code e aproveitamento de código
* Aplicação escalável com Nginx, fazendo proxypass para 3 instâncias Spring Boot
* Infra montada em apenas um comando com Docker Compose
* Controle e versionamento do banco com Liquibase
