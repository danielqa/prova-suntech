package br.com.suntech.prova.specification;

import br.com.suntech.prova.dto.UsersFiltersDto;
import br.com.suntech.prova.model.Users;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class UsersSpecification implements Specification<Users> {

    private final UsersFiltersDto dto;

    public UsersSpecification(UsersFiltersDto dto) {
        this.dto = dto;
    }

    @Override
    public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = this.applyFilters(root, cb);
        return BaseSpecification.andTogether(cb, predicates);
    }

    private List<Predicate> applyFilters(Root<Users> root, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<>();
        this.filterByUsername(root, cb, predicates);
        this.filterByName(root, cb, predicates);
        this.filterByEmail(root, cb, predicates);
        return predicates;
    }

    private void filterByUsername(Root<Users> root, CriteriaBuilder cb, List<Predicate> predicates) {
        String username = this.dto.getUsername();
        if (!StringUtils.isEmpty(username)) {
            predicates.add(BaseSpecification.filterLikeLowerCase(root, cb, "username", username));
        }
    }

    private void filterByName(Root<Users> root, CriteriaBuilder cb, List<Predicate> predicates) {
        String name = this.dto.getName();
        if (!StringUtils.isEmpty(name)) {
            predicates.add(BaseSpecification.filterLikeLowerCase(root, cb, "name", name));
        }
    }

    private void filterByEmail(Root<Users> root, CriteriaBuilder cb, List<Predicate> predicates) {
        String email = this.dto.getEmail();
        if (!StringUtils.isEmpty(email)) {
            predicates.add(BaseSpecification.filterEqual(root, cb, "email", email));
        }
    }
}