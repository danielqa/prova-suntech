package br.com.suntech.prova.dto;

import javax.ws.rs.QueryParam;

public class UsersFiltersDto {

    @QueryParam("username")
    private String username;

    @QueryParam("name")
    private String name;

    @QueryParam("email")
    private String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
