package br.com.suntech.prova.app;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("api")
public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {
        packages("br.com.suntech.prova", "com.fasterxml.jackson.jaxrs.json");
        register(CORSResponseFilter.class);
    }
}
