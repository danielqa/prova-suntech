package br.com.suntech.prova.specification;

import br.com.suntech.prova.model.Users;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

class BaseSpecification {

    static Predicate filterLikeLowerCase(Root<Users> root, CriteriaBuilder cb, String column, String value) {
        return cb.like(cb.lower(root.get(column)), "%" + value.toLowerCase() + "%");
    }

    static Predicate filterEqual(Root<Users> root, CriteriaBuilder cb, String column, String value) {
        return cb.equal(root.get(column), value);
    }

    static Predicate andTogether(CriteriaBuilder cb, List<Predicate> predicates) {
        return cb.and(predicates.toArray(new Predicate[0]));
    }
}