package br.com.suntech.prova.service;

import br.com.suntech.prova.dto.UsersFiltersDto;
import br.com.suntech.prova.dto.UsersResultDto;
import br.com.suntech.prova.model.Users;
import br.com.suntech.prova.repository.UsersRepository;
import br.com.suntech.prova.specification.UsersSpecification;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersService {

    private final UsersRepository repository;

    @Inject
    public UsersService(UsersRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    public List<UsersResultDto> findByFilters(final UsersFiltersDto dto) {
        List<Users> result = this.repository.findAll(new UsersSpecification(dto), Sort.by(Order.asc("id")));
        return result.stream().map(this::copyPropertiesToUsersResultDto).collect(Collectors.toList());
    }

    private UsersResultDto copyPropertiesToUsersResultDto(Users users) {
        UsersResultDto resultDto = new UsersResultDto();
        BeanUtils.copyProperties(users, resultDto);
        return resultDto;
    }
}
