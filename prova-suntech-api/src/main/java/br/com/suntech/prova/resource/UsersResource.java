package br.com.suntech.prova.resource;

import br.com.suntech.prova.dto.UsersFiltersDto;
import br.com.suntech.prova.dto.UsersResultDto;
import br.com.suntech.prova.service.UsersService;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.List;

@Resource
@Path("users")
public class UsersResource {

    private final UsersService service;

    @Inject
    public UsersResource(UsersService service) {
        this.service = service;
    }

    @GET
    @Path("filters")
    public List<UsersResultDto> findByFilters(@BeanParam final UsersFiltersDto dto) {
        return this.service.findByFilters(dto);
    }
}
