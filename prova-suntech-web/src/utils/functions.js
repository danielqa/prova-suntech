const LOCALE_DEFAULT = 'pt-BR';

export function flagEnabled(isEnabled) {
    return isEnabled ? 'Yes' : 'No';
}

export function formatDateTime(date) {
    if (!date) {
        return;
    }
    if (typeof date === 'string') {
        date = new Date(date);
    }
    return new Intl.DateTimeFormat(LOCALE_DEFAULT).format(date);
}