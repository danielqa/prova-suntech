import React, {Component} from 'react';
import Axios from 'axios';

import {flagEnabled, formatDateTime} from '../../utils/functions';

class UsersList extends Component {

    constructor(props) {
        super(props);
        this.state = {list: [], params: {}, isLoading: false};

        this.handleUsername = this.handleUsername.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.findByFilters();
    }

    handleUsername(event) {
        this.setState({params: {...this.state.params, username: event.target.value}});
    }

    handleName(event) {
        this.setState({params: {...this.state.params, name: event.target.value}});
    }

    handleEmail(event) {
        this.setState({params: {...this.state.params, email: event.target.value}});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.findByFilters();
    }

    findByFilters() {
        const {params} = this.state;
        this.setState({isLoading: true});
        Axios.get('api/users/filters', {params}).then(response => {
            setTimeout(() => {
                this.setState({list: response.data, isLoading: false});
            }, 500);
        });
    }

    tableBodyContent() {
        const {isLoading, list} = this.state;
        if (isLoading || !list.length) {
            return;
        }
        return (
            <tbody>
            {list.map(item => {
                return (
                    <tr key={item.id}>
                        <td>{item.id}</td>
                        <td>{item.username}</td>
                        <td>{item.password}</td>
                        <td>{flagEnabled(item.enabled)}</td>
                        <td>{formatDateTime(item.registerDate)}</td>
                        <td>{item.name}</td>
                        <td>{item.surname}</td>
                        <td>{item.email}</td>
                        <td>{item.phone}</td>
                    </tr>
                );
            })}
            </tbody>);
    }

    tableFooterContent() {
        const {isLoading, list} = this.state;
        if (!isLoading && list.length) {
            return;
        }
        return (
            <tfoot>
            <tr>
                <td colSpan="100">
                    {isLoading ? <i className="fa fa-spinner fa-2x fa-spin"></i> : 'No results found!'}
                </td>
            </tr>
            </tfoot>
        );
    }

    render() {
        return (
            <div>
                <h1 className="mt-3">Users list</h1>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-row">
                        <div className="form-group col-md-4">
                            <label htmlFor="inputUsername">Username</label>
                            <input type="text" className="form-control" id="inputUsername" placeholder="Username"
                                   onChange={this.handleUsername}/>
                        </div>
                        <div className="form-group col-md-4">
                            <label htmlFor="inputName">Name</label>
                            <input type="text" className="form-control" id="inputName" placeholder="Name"
                                   onChange={this.handleName}/>
                        </div>
                        <div className="form-group col-md-4">
                            <label htmlFor="inputEmail">Email</label>
                            <input type="email" className="form-control" id="inputEmail" placeholder="Email"
                                   onChange={this.handleEmail}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">Search</button>
                    </div>
                </form>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Enabled</th>
                        <th>Register Date</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                    </tr>
                    </thead>
                    {this.tableBodyContent()}
                    {this.tableFooterContent()}
                </table>
            </div>
        );
    }
}

export default UsersList;
