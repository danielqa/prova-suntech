import React, {Component} from 'react';
import UsersList from './components/users/UsersList';

class App extends Component {

    render() {
        return (
            <div className="container-fluid">
                <UsersList/>
            </div>
        );
    }
}

export default App;
